What?
-----

`biff` is a mildly-optimizing Brainfuck interpreter. It performs just enough
optimization to remain simple while making runtime reasonable:

* Consecutive `+`/`-` and `<`/`>` are collapsed
* Clearing a cell (`[-]`) is instant
* Data transfer loops (`[->+>-<<]`) are unrolled and executed in one step

`biff` also allows translation of a Brainfuck program to C source code amenable
to compilation with `gcc -O3`. C output is aided by the above optimizations,
but uses a fixed 8MB tape and includes no bounds checking.

Usage
-----

Interpret a program:
```
biff mandelbrot.b
```

Compile and run a program:
```
biff -c mandelbrot.b | gcc -x c -O3 -
./a.out
```

Download
--------

Pre-compiled executables can be [downloaded from Bintray](https://bintray.com/selb/go/biff).