package biff

import "fmt"

func (op operation) String() string {
	switch op {
	case opAdd:
		return "Add"
	case opMove:
		return "Move"
	case opLoop:
		return "Loop"
	case opInput:
		return "Input"
	case opOutput:
		return "Output"
	case opClear:
		return "Clear"
	case opTransfer:
		return "Transfer"
	}
	return "???"
}

func (i instruction) String() string {
	if i.op == opAdd {
		if i.count < 0 {
			return fmt.Sprintf("-%d", -i.count)
		} else {
			return fmt.Sprintf("+%d", i.count)
		}
	} else if i.op == opMove {
		if i.count < 0 {
			return fmt.Sprintf("<%d", -i.count)
		} else {
			return fmt.Sprintf(">%d", i.count)
		}
	} else {
		return fmt.Sprintf("%v*%d%v", i.op, i.count, i.body)
	}
}