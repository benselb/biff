package biff

import (
	"io"
)

type IOHandler interface {
	Input() (byte, error)
	Output(byte) error
}

func resizeTape(tape []byte, ptr int) []byte {
	for ptr >= len(tape) {
		tape = append(tape, 0)
	}
	return tape
}

type TapeUnderflowError struct{}
func (TapeUnderflowError) Error() string {
	return "Pointer moved past left end of tape"
}

func run(body []instruction, ioHandler IOHandler, tape []byte, ptr int) ([]byte, int, error) {
	for _, instr := range body {
		switch instr.op {
		case opAdd:
			tape[ptr] = byte((int(tape[ptr]) + instr.count) & 0xFF)
		case opMove:
			// Since moves are combined, the perhaps questionable behavior of temporarily
			// going off the end of the tape while moving is implicitly permitted.
			ptr += instr.count
			if ptr < 0 {
				return nil, 0, TapeUnderflowError{}
			}
			tape = resizeTape(tape, ptr)
		case opLoop:
			for tape[ptr] != 0 {
				var err error
				tape, ptr, err = run(instr.body, ioHandler, tape, ptr)
				if err != nil {
					return nil, 0, err
				}
			}
		case opInput:
			b, err := ioHandler.Input()
			if err == io.EOF {
				b = tape[ptr]
			} else if err != nil {
				return nil, 0, err
			}
			tape[ptr] = b
		case opOutput:
			err := ioHandler.Output(tape[ptr])
			if err != nil {
				return nil, 0, err
			}
		case opClear:
			tape[ptr] = 0
		case opTransfer:
			if tape[ptr] != 0 {
				effects := instr.transferEffects
				left := ptr + effects[0].cell
				right := ptr + effects[len(effects)-1].cell
				if left < 0 {
					return nil, 0, TapeUnderflowError{}
				}
				tape = resizeTape(tape, right)
				n := int(tape[ptr])
				for _, effect := range effects {
					tape[ptr+effect.cell] = byte((int(tape[ptr+effect.cell]) + n*effect.multiplier) & 0xFF)
				}
				tape[ptr] = 0
			}
		}
	}
	return tape, ptr, nil
}

func Run(program Program, ioHandler IOHandler) error {
	tape := make([]byte, 1, 1024)
	_, _, err := run(program, ioHandler, tape, 0)
	return err
}
