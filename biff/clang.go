package biff

import (
	"fmt"
	"strings"
)

func appendC(body []instruction, lines []string) []string {
	for _, instr := range body {
		var line string = ""
		switch instr.op {
		case opAdd:
			line = fmt.Sprintf("*ptr = (byte)(((word)*ptr + %d) & 0xFF);", instr.count)
		case opMove:
			line = fmt.Sprintf("ptr += %d;", instr.count)
		case opLoop:
			lines = append(lines, "while (*ptr) {")
			lines = appendC(instr.body, lines)
			lines = append(lines, "}")
		case opInput:
			line = "c = getc(stdin); if (c != EOF) *ptr = c;"
		case opOutput:
			line = "putc(*ptr, stdout);"
		case opClear:
			line = "*ptr = 0;"
		case opTransfer:
			lines = append(lines, "if (*ptr) {")
			for _, effect := range instr.transferEffects {
				lines = append(lines, fmt.Sprintf("ptr[%d] = (byte)(((word)ptr[%d] + ((word)*ptr)*%d) & 0xFF);", effect.cell, effect.cell, effect.multiplier))
			}
			lines = append(lines, "*ptr = 0;")
			lines = append(lines, "}")
		}
		if line != "" {
			lines = append(lines, line)
		}
	}
	return lines
}

func ProgramToC(program Program) string {
	lines := []string{
		"#include \"stdint.h\"",
		"#include \"stdio.h\"",
		"#include \"stdlib.h\"",
		"#include \"string.h\"",
		"#define TAPESIZE (8*1024*1024)",
		"typedef int_fast32_t word;",
		"typedef unsigned char byte;",
		"int main(void) {",
		"int c;",
		"byte *ptr = malloc(TAPESIZE);",
		"memset(ptr, TAPESIZE, 0);",
	}
	lines = appendC(program, lines)
	lines = append(lines, "return 0;")
	lines = append(lines, "}")
	return strings.Join(lines, "\n")
}
