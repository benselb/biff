package biff

import (
	"errors"
	"sort"
)

type operation int

const (
	// "Real" operations
	opAdd operation = iota
	opMove
	opLoop
	opInput
	opOutput

	// Internally-understood operations
	opClear
	opTransfer
)

type instruction struct {
	op    operation
	count int
	body  []instruction

	// The effects of an opTransfer, in ascending order by cell. When op=opTransfer,
	// this slice will contain at least one item.
	transferEffects []transferEffect
}

type transferEffect struct {
	cell       int
	multiplier int
}

type ByTransferCell []transferEffect
func (ls ByTransferCell) Len() int           { return len(ls) }
func (ls ByTransferCell) Less(i, j int) bool { return ls[i].cell < ls[j].cell }
func (ls ByTransferCell) Swap(i, j int)      { ls[i], ls[j] = ls[j], ls[i] }

type Program []instruction

func appendInstruction(body []instruction, op operation, count int) []instruction {
	var last *instruction = nil
	if len(body) > 0 {
		last = &body[len(body)-1]
	}
	if last != nil && last.op == op && (op == opAdd || op == opMove) {
		last.count += count
	} else {
		body = append(body, instruction{op: op, body: nil, count: count})
	}

	return body
}

// Optimize the usual [-] to a single instruction.
func optimizeClear(loop *instruction) {
	if loop.op == opLoop && len(loop.body) == 1 {
		instr := loop.body[0]
		if instr.op == opAdd && instr.count == -1 {
			loop.op = opClear
			loop.body = nil
		}
	}
}

// Optimize basic cell transfer loops.
func optimizeTransfer(loop *instruction) {
	if loop.op != opLoop {
		// Someone else has already optimized this loop.
		return
	}

	effects := make(map[int]int)
	ptr := 0
	for _, instr := range loop.body {
		switch instr.op {
		case opAdd:
			effects[ptr] += instr.count
		case opMove:
			ptr += instr.count
		default:
			// This isn't actually a basic loop.
			return
		}
	}
	if effects[0] == -1 && ptr == 0 {
		loop.op = opTransfer
		for cell, multiplier := range effects {
			if cell != 0 {
				loop.transferEffects = append(loop.transferEffects, transferEffect{cell: cell, multiplier: multiplier})
			}
		}
		if loop.transferEffects == nil {
			// Let later users assume that there is at least one effect
			loop.op = opClear
		} else {
			sort.Sort(ByTransferCell(loop.transferEffects))
		}
		loop.body = nil
	}
}

func Parse(source []byte) (Program, error) {
	body := make([]instruction, 0, 1024)
	bodyStack := make([][]instruction, 0, 16)
	mayBeTransfer := false
	for _, b := range source {
		switch b {
		case '+':
			body = appendInstruction(body, opAdd, 1)
		case '-':
			body = appendInstruction(body, opAdd, -1)
		case '>':
			body = appendInstruction(body, opMove, 1)
		case '<':
			body = appendInstruction(body, opMove, -1)
		case '[':
			bodyStack = append(bodyStack, body)
			body = make([]instruction, 0, 16)
			// A loop may be a copy if it contains only +-<>. Assume this one is eligible until proven otherwise.
			mayBeTransfer = true
		case ']':
			if len(bodyStack) == 0 {
				return nil, errors.New("Unmatched ]")
			}
			loop := instruction{op: opLoop, count: 1, body: body}
			optimizeClear(&loop)
			if mayBeTransfer {
				optimizeTransfer(&loop)
			}
			body = bodyStack[len(bodyStack)-1]
			bodyStack = bodyStack[:len(bodyStack)-1]
			body = append(body, loop)
			mayBeTransfer = false
		case ',':
			body = appendInstruction(body, opInput, 1)
			mayBeTransfer = false
		case '.':
			body = appendInstruction(body, opOutput, 1)
			mayBeTransfer = false
		}
	}

	if len(bodyStack) != 0 {
		return nil, errors.New("Unmatched [")
	}

	return Program(body), nil
}
