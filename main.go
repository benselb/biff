package main

import (
	"bitbucket.org/benselb/biff/biff"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

func fatal(doing string, err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed %s: %s\n", doing, err)
		os.Exit(1)
	}
}

type standardIO struct{}

func (standardIO) Input() (byte, error) {
	buf := []byte{0}
	_, err := os.Stdin.Read(buf)
	return buf[0], err
}
func (standardIO) Output(b byte) error {
	buf := []byte{b}
	_, err := os.Stdout.Write(buf)
	return err
}

func main() {
	outputC := false
	flag.BoolVar(&outputC, "c", false, "output C code")
	flag.Parse()

	fnames := flag.Args()
	if len(fnames) != 1 {
		fmt.Fprintf(os.Stderr, "Usage: %s [-ch] program.bf\n", os.Args[0])
		os.Exit(1)
	}

	f, err := os.Open(fnames[0])
	fatal("opening "+fnames[0], err)

	buf, err := ioutil.ReadAll(f)
	fatal("reading "+f.Name(), err)

	program, err := biff.Parse(buf)
	fatal("parsing "+f.Name(), err)

	if outputC {
		fmt.Println(biff.ProgramToC(program))
	} else {
		err = biff.Run(program, standardIO{})
		fatal("running "+f.Name(), err)
	}
}
